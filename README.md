### CFSSL Docker Image for lightweight Certificate Authority 

CFSSL (https://github.com/cloudflare/cfssl) is a collection of open source PKI and TLS tools created by CloudFlare (https://www.cloudflare.com/) which known primarily for their website acceleration and protection services.

A key feature of the cfssl utility is it’s api server functionality that allows certificates to be generated via a REST API call and will return the certificate data in a JSON response.

### Usage

```
docker run -d -p 8888:8888 cfssl
```

Create sample bash script to generate certificates change appropriate names 

```
sh generatecert.sh servername.domain.local 192.168.56.10
```

```
#!/bin/bash


certname=$1
caaddress=$2
#set -x
# Generate Certificate
curl -d '{ "request": {"hosts":["$certname"],
"names":[{"C":"MM", "ST":"Yangon", "L":"Yangon", "O":"test.local"}]} }' http://$caaddress:8888/api/v1/cfssl/newcert > tmpcert.json

# Create Private Key
echo -e "$(cat tmpcert.json | python -m json.tool |
        grep private_key | cut -f4 -d '"')"  > ./$certname.key

# Create Certificate
echo -e "$(cat tmpcert.json | python -m json.tool |
        grep -m 1 certificate | cut -f4 -d '"')"  > ./$certname.cer

# Create Certificate Request
echo -e "$(cat tmpcert.json | python -m json.tool |
        grep certificate_request | cut -f4 -d '"')" > ./$certname.csr

# Remove JSON Data
rm -Rf tmpcert.json
```

###### Ref
https://www.greenreedtech.com/building-a-lightweight-ceritifcate-authority/
